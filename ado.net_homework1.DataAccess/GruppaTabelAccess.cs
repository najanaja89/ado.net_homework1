﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ado.net_homework1.DataAccess
{
    public class GruppaTabelAccess
    {
        private readonly string _connectionString;

        public GruppaTabelAccess()
        {
            _connectionString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=D:\Source\ado.net\ado.net_homework1\ado.net_homework1.DataAccess\gruppa.mdf;Integrated Security=True;User Instance=True";
        }

        public void CreateTable()
        {
            //var data = new List<User>();
            string query = @"CREATE TABLE gruppa (id int IDENTITY(1,1),name nvarchar(50))";

            using (var connection = new SqlConnection(_connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            {

                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (SqlException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
            }
        }
    }
}

